<?php
/*
* Template Name: Home Page
*/
get_header();
$templateDirectorty = esc_url(get_template_directory_uri());
?>
<div class="container">
    <div class="row" >
        <div class="col-xs-12" style="height: 480px; background-color: #ffffff">
            <div id="header-slides">
                <?php
                echo do_shortcode('[layerslider id="1"]');

                ?>
            </div>
            <div class="col-xs-4 col-xs-offset-4 btn-scrolling-down">
                <a href="#about-us" rel="m_PageScroll2id"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/scrolling_arrow.png"></a>
            </div>
        </div>
    </div>
    <?php
    $args = array(
        'numberposts' => 1,
        'post_type' => 'aboutus'
    );

    $postslist = get_posts($args);
    if ($postslist) {
        foreach ($postslist as $post) {
            setup_postdata($post);
            ?>
            <div class="row" id="about-us">
                <div class="col-xs-12 about-us time-investments" style="background-color: green">
                    <ul>
                        <li class="au-investments">
                            <span data-num="<?php the_field('invest-portfolio');?>"><?php the_field('invest-portfolio');?></span>
                            <p>INVESTMENTS</p>
                        </li>
                        <li class="au-investments-year">
                            <span data-num="<?php the_field('inestments-year-portfolio');?>"><?php the_field('inestments-year-portfolio');?>+</span>
                            <p>INVESTMENTS</p>
                            <p>A YEAR</p>
                        </li>

                    </ul>
                    <!--            <div>-->
                    <!--                <img class="" src="-->
                    <?php //echo esc_url(get_template_directory_uri()); ?><!--/images/background_about1.png">-->
                    <!--            </div>-->
                </div>

            </div>
            <div class="row">
                <div class="col-xs-12 about-us time-exists" style="background-color: green ; padding-bottom: 66px;">
                    <ul>
                        <li class="au-in-exists">
                            <span data-num="<?php the_field('exits-portfolio');?>">$<?php the_field('exits-portfolio');?>M</span>
                            <p>IN EXISTS</p>
                            <p>(PAST 24 MONTHS)</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 au-content" style="background-color: #ffffff">

                    <p class="au-content-first">ABOUT US</p>

                    <p class="au-content-second"><?php the_title();?></p>
                    <!--            <div class="col-xs-1 team-content border-active">-->
                    <!---->
                    <!--            </div>-->
                    <p class="au-active"></p>
                    <div class="au-content-description">
                        <?php the_content();?>
                    </div>
<!--                    <p class="au-content-third">Revel Partners is an early-expansion stage fund focused on financing disruptive innovation in the areas of digital media & internet technology. We are a close team of entrepreneurs & investors operating out of New York City. Our proven expertise in digital media innovation, venture capital investing, and our global network of relationships in the industry can empower strong management teams with good ideas to rapidly accelerate their growth. Revel portfolio companies maximize value by not only leveraging our financial investment but also our extensive networks of relationships within the digital ecosystem.</p>-->
                </div>
            </div>
        <?php
        }
    }
    ?>

    <?php
    $args = array(
        'numberposts' => 1,
        'post_type' => 'portfolio'
    );
    $postslist = get_posts($args);
    if ($postslist) {
    foreach ($postslist as $post) {
    setup_postdata($post);
            ?>
        <div class="row" id="portfolio">
            <div class="col-xs-12 our-portfolio" style="background-color: #ffff00">
                <div class="portfolio-header">
                    <p>OUR</p>
                    <span>PORTFOLIO</span>
                </div>
                <div class="portfolio-active"></div>
                <div class="portfolio-content">
                    <?php the_content();?>
<!--                    <p>Our portfolio includes a roster of talented entrepreneurs and management teams who are developing novel products &services that change the way the industry operates. We are proud to partner with some of the best and brightest emerging leaders in the digital media and internet technology sectors as they build market defining, global companies. Our portfolio currently includes:</p>-->
                </div>
            </div>
        </div>

    <?php
        }
    }
    ?>

<!--    Filter not complete so not show-->
    <div class="row page03-row-03">
        <div class="col-xs-12">
            <ul class="list-inline">
                <li class="filter">FILTER:</li>
                <li><a filter="ALL">ALL</a></li>
                <li><a filter="ADTECH">ADTECH</a></li>
                <li><a filter="CONSUMER">CONSUMER</a></li>
                <li><a filter="ECOMERCE">ECOMERCE</a></li>
                <li><a filter="ENTERPRISE">ENTERPRISE</a></li>
                <li><a filter="INFRASTRUCTURE">INFRASTRUCTURE</a></li>
                <li><a filter="EXISTS">EXISTS</a></li>
            </ul>
        </div>
    </div>
    <?php
    $args = array(
        'post_type' => 'our-investment'
    );
    $postslist = get_posts($args);
    if ($postslist) {
    foreach ($postslist as $post) {
    setup_postdata($post);
    ?>
        <div class="row">
            <div class="col-xs-12 portfolio-filter" style="background-color: #ffffff">
                <div class="portfolio-logo">
                    <div class="col-xs-2 border-left"></div>
                    <div class="col-xs-8 image portfolio-block" category="<?php the_field('category-investment') ?>">
                        <img src="<?php the_field('avatar-investment') ?>">
                    </div>
                    <div class="col-xs-2 border-right"></div>
                </div>
            </div>
        </div>
    <?php
        }
    }
    ?>

    <div id="portfolio" class="modal fade portfolio-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content container-fluid">
                <div class="row">
                    <div class="page03-row-02-slides col-xs-12">
                        <?php
                        $args = array(
                            'post_type' => 'our-investment'
                        );
                        $postslist = get_posts($args);
                        if ($postslist) {
                            foreach ($postslist as $post) {
                                setup_postdata($post);
                                ?>
                                <div>
                                    <div class="col-xs-12" style="padding-left: 0; padding-right: 0">
                                        <div class="portfolio-image">
                                            <div class="col-xs-10">
                                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/portfolio_slide_image.png">
                                            </div>
                                            <div class="col-xs-2 portfolio-slide-close-btn" data-dismiss="modal">
                                                <a href="#">
                                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/portfolio_slide_close_btn.png">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="page03-slide">

                                            <div class="col-xs-12">
                                                <ul class="list-inline portfolio-title">
                                                    <li>
                                                        <span class="portfolio-title-prefix">CEO: </span><span class="portfolio-title-suffix"><?php the_field('ceo-investment') ?></span>
                                                    </li>
                                                    <li>
                                                        <span class="portfolio-title-prefix">LOCATION: </span><span class="portfolio-title-suffix"><?php the_field('location-investment') ?></span>
                                                    </li>
                                                    <li>
                                                        <span class="portfolio-title-prefix">WEBSITE: </span><span class="portfolio-title-suffix"><?php the_field('website-investment') ?></span>
                                                    </li>
                                                </ul>
                                                <br/>
                                                <p class="portfolio-title-content">
                                                    Business: Tracx is a NYC-based company with a SaaS platform for
                                                    sophisticated brand marketers who want to do more than monitor their
                                                    social media presence, but actually manage it. The company provides an
                                                    end-to-end solution that indexes the entire social web and delivers the
                                                    most relevant, high impact audiences and conversations by capturing a
                                                    360 degree view of activity around a brand. The platform allows marketers
                                                    to sift through streams of social media data and drill down to provide
                                                    geographic, demographic, and psychographic insights and to monitor
                                                    performance against competitors while planning, monitoring, engaging, and
                                                    measuring influencers all in one place. For more information, visit
                                                    www.tracx.com.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row" id="team">
        <div class="col-xs-12 team" style="background-color: #06B9FD">
            <div class="team-header">
                <p>MEET THE</p>
                <span>TEAM</span>
            </div>
            <div class="col-xs-1 team-content border-active">

            </div>
        </div>

    </div>
    <?php
    $args = array(
        'post_type' => 'team',
        'meta_key'	=> 'order-team',
        'orderby'	=> 'meta_value_num',
        'order'    => 'ASC'
    );
    $postslist = get_posts($args);
    if ($postslist) {
        foreach ($postslist as $post) {
            setup_postdata($post);
            ?>
            <div class="row">
                <div class="col-xs-12 team-image team-block">
                    <img src="<?php the_field('photo-member'); ?>" logo-detail="<?php the_field('photo-mamber-details'); ?>" member-name="<?php the_title()?>" member-background="<?php the_content();?>" member-investment="<?php the_field('investments');?>">
                </div>
                <div class="col-xs-7 col-xs-offset-3 team-content">
                    <span><?php the_title(); ?></span>
                </div>
                <div class="col-xs-2 team-logo">
                    <span><?php the_field('linkin'); ?></span>
                </div>
            </div>
        <?php
        }
    }
    ?>

    <div id="team" class="modal fade team-popup">
        <div class="modal-dialog modal-lg">
            <div class="modal-content container-fluid">
                <div class="row">
                    <div class="page04-row-02-slides col-xs-12" style="padding-left: 0; padding-right: 0;">
                        <?php
                        $args = array(
                            'post_type' => 'team',
                            'meta_key'	=> 'order-team',
                            'orderby'	=> 'meta_value_num',
                            'order'    => 'ASC'
                        );
                        $postslist = get_posts($args);
                        if ($postslist) {
                            foreach ($postslist as $post) {
                                setup_postdata($post);
                                ?>
                                <div>
                                    <div class="col-xs-12" style="padding-left: 0; padding-right: 0;">
<!--                                        <div class="col-xs-10 team-detail">-->
<!---->
<!--                                        </div>-->
                                        <div>
                                            <img src="<?php the_field('photo-mamber-details'); ?>" width="100%">
                                        </div>
                                        <div class="col-xs-2 team-slide-close-btn" data-dismiss="modal">
                                            <a href="#">
                                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/portfolio_slide_close_btn.png">
                                            </a>
                                        </div>
                                        <div class="team-slide">
                                            <div>
                                                <p class="team-detail-member"><?php the_title(); ?></p>

                                                <p class="team-detail-background">BACKGROUND</p>

                                                <div class="team-detail-content">
                                                    <?php the_content(); ?>
                                                </div>
<!--                                                <p class="team-detail-content">Thomas is the CEO of eValue Group, Germany’s leading-->
<!--                                                    super angel investment-->
<!--                                                    group and digital media incubator. Through the eValue Group, Thomas invested-->
<!--                                                    in smartclip AG, Europe’s largest online video advertising network, United Mail-->
<!--                                                    Solutions,-->
<!--                                                    a leading European email marketing solutions provider, ClickDistrict,-->
<!--                                                    a real-time bidding platform, Voodoo Video, a long-tail video distribution-->
<!--                                                    network,-->
<!--                                                    and StrikeAd, a mobile demand side platform for digital agencies.</p>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row" id="contact">
        <div class="col-xs-12 contact" style="background-color: #06B9FD">
            <div class="contact-header">
                <p>GET IN</p>
                <span>CONTACT</span>
            </div>
            <div class="team-content col-xs-1 border-active">

            </div>
        </div>
        <div class="col-xs-12">
            <div class="row contact-content">
                <div class="col-xs-2 contact-image"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/contact-place.png"></div>
                <div class="col-xs-10 contact-place">
                    <p class="country">UNITED STATES</p>
                    <p class="address">229W, 43rd Street, 8th Floor</p>
                    <p class="address">New York, NY 10036</p>
                </div>
            </div>
            <div class="row contact-content">
                <div class="col-xs-2 contact-image"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/contact-place.png"></div>
                <div class="col-xs-10 contact-place">
                    <p class="country">GERMANY</p>
                    <p class="address">Kennedydamm 1</p>
                    <p class="address">40476 Dusseldorf</p>
                    <p class="address">T +49.21.520.9940</p>
                </div>
            </div>
        </div>


    </div>
    <div class="row contact-send-mail">
        <div class="col-xs-12 send-mail-header">
            <img class="" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/contact-mail.png">
            <span class="">DROP US A LINE</span
        </div>
        <div class="col-xs-12 send-mail-content">
            <input type="text" class="form-control your-name" placeholder="Your Name">
            <input type="text" class="form-control your-mail" placeholder="Your Mail">
            <textarea rows="12"
                      class="form-control your-message"
                      placeholder="Your Message"></textarea>
            <button type="button" class="btn btn-primary btn-send-mail">SEND MESSAGE</button>
        </div>
    </div>

</div>

<?php get_footer(); ?>
