<?php
    /**
     * Your Twitter App Info
     */
    
    // Consumer Key
    define('CONSUMER_KEY', get_field("twitter_consumer_key", "option"));
    define('CONSUMER_SECRET', get_field("twitter_consumer_secret", "option"));

    // User Access Token
    define('ACCESS_TOKEN', get_field("twitter_access_token", "option"));
    define('ACCESS_SECRET', get_field("twitter_access_secret", "option"));
	
	// Cache Settings
	define('CACHE_ENABLED', false);
	define('CACHE_LIFETIME', 3600); // in seconds
	define('HASH_SALT', md5(dirname(__FILE__)));