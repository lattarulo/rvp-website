var sliderHeight = 100;
var currentLayerIndex = false;
var layersliderInnerClass = '.ls-inner';

function superliderInit(){
    $('.ls-nav-prev').after('<a class="ls-nav-divide"></a>');
    $("div.m_PageScroll2id").each(function(){$(this).parent().mPageScroll2id()});
}

function updateHomeSliderBackground(data){
    sliderHeight = $(layersliderInnerClass).height();

    currentLayerIndex = data.nextLayerIndex == 2;

    if(currentLayerIndex){ //slider number 2
        //change background color to white
       $(layersliderInnerClass).css('background-color', '#fff');
        //change navbar-default background-color and text-color
        if( jQuery(".navbar").offset().top < sliderHeight){
            $('.navbar-default').addClass('light-bg');
            $('img#logo-header').attr('src', templateUrl + '/images/rvp-logo.png');
        }else{
            $('.navbar-default').removeClass('light-bg');
            $('img#logo-header').attr('src', templateUrl + '/images/rvp-logo.png');
        }

        $('.ls-edffullwidth').addClass('ls-light-bg');
    }else
    {
        //change background color to #curLayerIndex
        $(layersliderInnerClass).css('background-color', 'rgb(9, 25, 39)');
        //change navbar-default background-color and text-color
        $('.navbar-default').removeClass('light-bg');
        $('img#logo-header').attr('src', templateUrl + '/images/rvp-logo.png');
        $('.ls-edffullwidth').removeClass('ls-light-bg');

    }
}
function updateNavbar(){
    if (jQuery(".navbar").offset().top < 100) {
        jQuery(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        jQuery(".navbar-fixed-top").removeClass("top-nav-collapse");
    }

    if( jQuery(".navbar").offset().top > sliderHeight){
        $('.navbar-default').removeClass('light-bg');
        $('img#logo-header').attr('src', templateUrl + '/images/rvp-logo.png');
    }else{
        if(currentLayerIndex){
            $('.navbar-default').addClass('light-bg');
            $('img#logo-header').attr('src', templateUrl + '/images/rvp-logo.png');
        }
    }
}
$(window).scroll(function () {
    updateNavbar();
});

$(window).on('resize', function() {
    if(searchInputShow)
    {
        if($('.navbar-header').width() > 150 && $('#search-form-full').is(":visible")){
            $('#search-form-full').hide();
            $('#search-form-collapse').show().animate({marginRight: 0},{ duration:500});
        }else if ($('.navbar-header').width() < 150 && $('#search-form-collapse').is(":visible")){
            $('#search-form-full').show().animate({marginRight: 0},{ duration:500});
            $('#search-form-collapse').hide();
        }
    }
});
var searchInputShow = false;
$(document).ready(function(){
    updateNavbar();
    $('body').pointer();

    $('.findout-more').hover(function(){
        $(this).css('background','#4D5B6B').addClass('hover');
    }, function(){
        $(this).css('background','#303d4a').removeClass('hover');

    });


    $('.navbar-default .search a').text('').addClass('glyphicon glyphicon-search');
    $('.navbar-default .search a').click(function() {
        searchInputShow = !searchInputShow;
        var searchformSelector = '#search-form-full';
        if($('.navbar-header').width() > 150)
        {
            searchformSelector = '#search-form-collapse';
        }
        if(searchInputShow) {
            $(searchformSelector).show()
                .animate({marginRight: 0}, {
                duration: 500,
                done: function() {
                    $('.search a').removeClass('glyphicon-search')
                                  .addClass('glyphicon-remove');
                }
            });
        } else {
            $(searchformSelector).animate({marginRight: -200}, {
                duration: 500,
                done: function() {
                    $(searchformSelector).hide();
                    $('.search a').removeClass('glyphicon-remove')
                                  .addClass('glyphicon-search');
                }
            });
        }
    });
});

(function($){
    $.fn.pointer = function (options) {
        var settings = {
            size : 50,
            spd : 300,
            color : "#165C98"
        }
        settings = $.extend(settings, options);

        var circle_style = {
            "position":"absolute",
            "z-index":9999,
            "height":10,
            "width":10,
            "border":"solid 4px "+settings.color,
            "border-radius":settings.size
        }
        return this.each(function() {
            var $this = $(this);
            $this.css({"position":"relative"});
            $(document).click(function(e){
                var x = e.pageX-5;
                var y = e.pageY-5;

                var pos = {
                    top :(-settings.size/2)+y,
                    left :(-settings.size/2)+x
                }

                $this.append('<div class="circle"></div>');
                $this.find(".circle:last").css(circle_style).css({
                    "top":y,
                    "left":x
                }).animate({"height":settings.size,"width":settings.size,"left":pos.left,"top":pos.top},{duration:settings.spd,queue:false})
                    .fadeOut(settings.spd*1.8,function(){
                        $(this).remove();
                    });
            });
        });
    }
})(jQuery);