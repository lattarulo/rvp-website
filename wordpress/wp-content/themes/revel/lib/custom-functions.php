<?php 

function localize_scripts() {
	$params = array(
		"siteUrl" => home_url(),
		"templateUrl" => get_template_directory_uri(),
		"ajaxUrl" => home_url()."/wp-admin/admin-ajax.php"
		);
	wp_localize_script('script', 'SiteVars', $params);
}

function get_slides(){
	return new WP_Query( array(
		"post_type"			=> 'slide',
		"posts_per_page"	=> 8
	) );
}

function get_child_pages( $ID ){
	$args = array(
	    'post_type'      => 'page',
	    'posts_per_page' => -1,
	    'post_parent'    => $ID,
	    'order'          => 'ASC',
	    'orderby'        => 'menu_order'
 	);
 	return new WP_Query( $args );
}

function get_portfolio(){
	return new WP_Query( array(
		'post_type'			=> 'portfolio',
		'posts_per_page'	=> -1,
		'order'          	=> 'ASC',
	    'orderby'        	=> 'menu_order'
	) );
}

function get_team(){
	return new WP_Query( array(
		'post_type'			=> 'team',
		'posts_per_page'	=> -1,
		'order'          	=> 'ASC',
	    'orderby'        	=> 'menu_order'
	) );
}


add_action('wp_ajax_get_ajax_tweet', 'get_ajax_tweet');
add_action('wp_ajax_nopriv_get_ajax_tweet', 'get_ajax_tweet');
 
function get_ajax_tweet() {
	 /**
     * Your Twitter App Info
     */
    
    // Consumer Key
    define('CONSUMER_KEY', get_field("twitter_consumer_key", "option"));
    define('CONSUMER_SECRET', get_field("twitter_consumer_secret", "option"));

    // User Access Token
    define('ACCESS_TOKEN', get_field("twitter_access_token", "option"));
    define('ACCESS_SECRET', get_field("twitter_access_secret", "option"));
	
	// Cache Settings
	define('CACHE_ENABLED', false);
	define('CACHE_LIFETIME', 3600); // in seconds
	define('HASH_SALT', md5(dirname(__FILE__)));

	require_once( "twitter_api/twitteroauth/twitteroauth.php" );
	require_once( "twitter_api/tweet.php" );

	die();
}

