<?php
/**
 * Template Name: About Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage revel
 * @since revel 1.0
 */

?>
<section id="<?php the_field("section_id"); ?>" class="about-page">
	<div class="container-fluid">
		<?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(), "full"); ?>
		<div class="col-md-3 parallax about-bg" >
			<div class="about-page-parallax" style="background-image: url('<?php echo $img[0]; ?>');"></div>
			<?php if( have_rows("numbers") ): ?>
				<?php $counter = count( get_field("numbers") ); ?>
				<?php while( have_rows("numbers") ): the_row(); ?>
					<div class="counter col-sm-4 col-md-12">

								<span class="number"><?php the_sub_field("before_number"); ?><span class="count" data-count="<?php the_sub_field("number"); ?>">0.0</span><?php the_sub_field("after_number"); ?></span>
								<?php the_sub_field("under_number"); ?>
							
					</div>
				<?php endwhile; ?>
				
			<?php endif; ?>
		</div>
		<div class="col-md-6 about-content">
			
					<h4><?php the_field("subtitle"); ?></h4>

					<?php if( get_field("sub_sub_title") ): ?>
						<h2 class="section-title has-sub-sub"><?php the_title(); ?></h2>
						<h3 class="sub-sub-title"><?php the_field("sub_sub_title"); ?></h3>
					<?php else: ?>
						<h2 class="section-title"><?php the_title(); ?></h2>
					<?php endif; ?>

					<?php the_content(); ?>
				
		</div>
	</div>
</section>