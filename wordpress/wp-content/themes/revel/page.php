<?php
/**
 * The static page template.
 *
 *
 * @package WordPress
 * @subpackage revel
 * @since revel 1.0
 */

get_header(); the_post(); ?>

	<main class="content full-width">
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
		<?php wp_link_pages(); ?>
	</main>

<?php get_footer(); ?>