function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}

function footerSetUp() {
    //var div = "<div style='text-align: center; font-family: tahoma, arial, sans serif; margin-top: 8px; font-size: 11px; font-weight:normal'><span style='color: #999'>Powered by </span><a href='"+StartUpHireUrl+"' target='_top' style='text-decoration: none'><span style='color: #2745be; font-weight:normal; font-size: 11px;'>StartUp</span><span style='color: #c1bb20; font-weight:normal; font-size: 11px;'>Hire</span></a></div>";
    //var footer = document.getElementById('poweredByStartUpHire');
    //if(footer) {
    //    footer.innerHTML = div;
    //} else {
    //    var appendDiv = document.createElement('div');
    //    appendDiv.innerHTML = "<div style='text-align: center; font-family: tahoma, arial, sans serif; margin-top: 8px; font-size: 11px; font-weight:normal'><span style='color: #999'>Powered by </span><a href='"+StartUpHireUrl+"' target='_top' style='text-decoration: none'><span style='color: #2745be; font-weight:normal; font-size: 11px;'>StartUp</span><span style='color: #c1bb20; font-weight:normal; font-size: 11px;'>Hire</span></a></div>";
    //    document.getElementById('StartUpHireIframe').parentNode.appendChild(appendDiv);
    //}
}

///////////////////////////////
// Begin main

var parameters = "searchId="+StartUpHireSearchId;
if(typeof StartUpHireVersion != 'undefined') {
    parameters += "&version="+StartUpHireVersion;
} else {
    parameters += "&version=1";
}
if(typeof StartUpHireSiteTag != 'undefined') {
    parameters += "&tag="+StartUpHireSiteTag;
}
if(typeof StartUpHirePrimaryColor != 'undefined') {
    parameters += "&primaryColor="+StartUpHirePrimaryColor;
}
if(typeof StartUpHireSecondaryColor != 'undefined') {
    parameters += "&secondaryColor="+StartUpHireSecondaryColor;
}
if(typeof StartUpHireLinkColor != 'undefined') {
    parameters += "&linkColor="+StartUpHireLinkColor;
}
if(typeof StartUpHireTextColor != 'undefined') {
    parameters += "&textColor="+StartUpHireTextColor;
}
if(typeof StartUpHireSearchFields != 'undefined') {
    for(var i=0; i<StartUpHireSearchFields.length; i++) {
        parameters += "&searchFields[]="+StartUpHireSearchFields[i];
    }
}
if(typeof StartUpHireColumns != 'undefined') {
    for(var i=0; i<StartUpHireColumns.length; i++) {
        parameters += "&columns[]="+StartUpHireColumns[i];
    }
}
if(typeof StartUpHireSortBy != 'undefined') {
    parameters += "&sortBy="+StartUpHireSortBy;
}
if(typeof StartUpHireAscDesc != 'undefined') {
    parameters += "&ascDesc="+StartUpHireAscDesc;
}
if(typeof StartUpHireShowJobs != 'undefined') {
    parameters += "&showJobs="+StartUpHireShowJobs;
}
if(typeof StartUpHireCSS != 'undefined') {
    parameters += "&css="+StartUpHireCSS;
}
if(typeof StartUpHirePeopleMaps != 'undefined') {
    parameters += "&peopleMaps="+StartUpHirePeopleMaps;
}
if(typeof StartUpHireExtra != 'undefined') {
    parameters += "&extra="+StartUpHireExtra;
}
if(typeof StartUpHireGoogleFont != 'undefined') {
    parameters += "&googleFont="+StartUpHireGoogleFont;
}
if(typeof StartUpHireLinkType != 'undefined' && StartUpHireLinkType == 'External') {
    parameters += "&linkType="+StartUpHireLinkType;
}

if(typeof StartUpHireHeight != 'undefined') {
    var StartUpHireHeight = StartUpHireHeight+"px";
} else if(typeof StartUpHireShowJobs != 'undefined') {
    var StartUpHireHeight = Number(StartUpHireShowJobs)*36+140;
    StartUpHireHeight = StartUpHireHeight+'px';
} else {
    var StartUpHireHeight = '760px';
}
if(typeof StartUpHireFixedHeight != 'undefined') {
    parameters += "&fixedHeight="+StartUpHireFixedHeight;
}
if(typeof StartUpHireKeywords != 'undefined' && StartUpHireKeywords != '') {
    parameters += "&keywords="+StartUpHireKeywords;
}
if(typeof StartUpHireLocation != 'undefined' && StartUpHireLocation != '') {
    parameters += "&location="+StartUpHireLocation;
}
if(typeof StartUpHireRadius != 'undefined' && StartUpHireRadius != '') {
    parameters += "&radius="+StartUpHireRadius;
}

// For dev/testing, this allows one to pass in the site's URL
if(typeof StartUpHireUrl == 'undefined' || StartUpHireUrl == '') {
    StartUpHireUrl = "http://www.startuphire.com";
}

// and now we can track whose widget this is:
parameters +="&hoster=" + window.location.hostname;


// Determine the version
if(typeof StartUpHireVersion != 'undefined' && StartUpHireVersion >= 2) {
    parameters += "&url="+encodeURIComponent(document.location);
    var StartUpHireWidgetHeight = document.location.hash.substr(1);
    function pollAndResize() {
        if(document.location.hash != '' && document.location.hash != '#') {
            var newHeight = document.location.hash.substr(1);

            if(newHeight != StartUpHireWidgetHeight && newHeight != '' && newHeight >= 0) {
                document.getElementById('StartUpHireIframe').setAttribute('height', newHeight+"px");
                document.location.hash = '';
                StartUpHireWidgetHeight = newHeight;
            }
        }
    }

    //copy of V3 to cover V4's new path
    if(StartUpHireVersion >= 4) {
        document.write("<iframe width='100%' height='"+StartUpHireHeight+"' marginheight='0px' marginwidth='0px' id='StartUpHireIframe' src='' frameborder='0'></iframe>");

        // when the page is done loading, do this.
        addLoadEvent(function() {
            var fail = false;

            // version 3+ requires the badge
            if(StartUpHireVersion >= 3) {
                var badge = document.getElementById('poweredByStartUpHire');
                if(badge) {
                    var anchor = badge.getElementsByTagName('a')[0];
                    if(anchor) {
                        var href = anchor.getAttribute('href');
                        var check = href.indexOf('http://www.startuphire.com');
                        if(check != 0) {
                            var fail = true;
                        }
                    } else {
                        var fail = true;
                    }
                } else {
                    var fail = true;
                }
            }

            var iFrame = document.getElementById('StartUpHireIframe');
            if(fail) {
                iFrame.setAttribute('src',StartUpHireUrl+"/widgetV4/error.php");
            } else {
                iFrame.setAttribute('src',StartUpHireUrl+"/widgetV4/?"+parameters);
                footerSetUp();
                var poll = window.setInterval('pollAndResize()', 100);
            }
        });
    }
    // version 3+ requires the badge (note that v3 uses V2 code, tho)
    else if(StartUpHireVersion >= 3) {
        document.write("<iframe width='100%' height='"+StartUpHireHeight+"' marginheight='0px' marginwidth='0px' id='StartUpHireIframe' src='' frameborder='0'></iframe>");

        // when the page is done loading, do this.
        addLoadEvent(function() {
            var fail = false;

            // version 3 requires the badge
            if(StartUpHireVersion >= 3) {
                var badge = document.getElementById('poweredByStartUpHire');
                if(badge) {
                    var anchor = badge.getElementsByTagName('a')[0];
                    if(anchor) {
                        var href = anchor.getAttribute('href');
                        var check = href.indexOf('http://www.startuphire.com');
                        if(check != 0) {
                            var fail = true;
                        }
                    } else {
                        var fail = true;
                    }
                } else {
                    var fail = true;
                }
            }
            var iFrame = document.getElementById('StartUpHireIframe');
            if(fail) {
                iFrame.setAttribute('src',StartUpHireUrl+"/widgetV2/error.php");
            } else {
                //console.log(StartUpHireUrl+"/widgetV2/?"+parameters);
                iFrame.setAttribute('src',StartUpHireUrl+"/widgetV2/?"+parameters);
                footerSetUp();
                var poll = window.setInterval('pollAndResize()', 100);
            }
        });
    } else {
        document.write("<iframe width='100%' height='"+StartUpHireHeight+"' marginheight='0px' marginwidth='0px' id='StartUpHireIframe' src='"+StartUpHireUrl+"/widgetV2/?"+parameters+"' frameborder='0'></iframe>");
        addLoadEvent(footerSetUp);
        var poll = window.setInterval('pollAndResize()', 100);
    }
} else {
    document.write("<iframe width='100%' height='"+StartUpHireHeight+"' marginheight='0px' marginwidth='0px' id='StartUpHireIframe' src='"+StartUpHireUrl+"/widget/?"+parameters+"' frameborder='0'></iframe>");
    addLoadEvent(footerSetUp);
}