<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 */
?>


<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery-1.11.2.min.js"></script>
<!--<script type="text/javascript" src="--><?php //echo esc_url( get_template_directory_uri() ); ?><!--/js/jquery.mobile-1.4.5.js"></script>-->
<!--<script type="text/javascript" src="--><?php //echo esc_url( get_template_directory_uri() ); ?><!--/js/jquery.mobile-1.4.5.min.js"></script>-->
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/app.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/bower_components/fullpage/jquery.fullPage.js" charset="utf-8"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/bower_components/jquery-animatenumber/jquery.animateNumber.js"></script>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/bower_components/slick.js/slick/slick.js"></script>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.malihu.PageScroll2id.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/main.js"></script>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/app.js"></script>
<?php wp_footer(); ?>

</body>
</html>
