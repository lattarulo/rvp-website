<?php
/**
 * Template Name: Jobs Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage revel
 * @since revel 1.0
 */

?>
<section id="<?php the_field("section_id"); ?>" class="jobs-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if( get_field("subtitle") ): ?>
					<h4><?php the_field("subtitle"); ?></h4>
				<?php endif; ?>
				<h2 class="section-title"><?php the_title(); ?></h2>


				<div class="iframe-wrapper">

					<script type="text/javascript" charset="utf-8">
					    // StartUpHire Widget Configuration Variables
					    var StartUpHireSearchId = "cc8d27a36edb1703bb091dffaaa570cc";
					    var StartUpHireVersion = 4;
					    var StartUpHireTag = "Revel Partners";
					    var StartUpHirePrimaryColor = "1c7bb0";
					    var StartUpHireSecondaryColor = "eff8f8";
					    var StartUpHireSearchFields = ["Keywords","Location"];
					    var StartUpHireColumns = ["Job Title","Company","Location"];
					    var StartUpHireSortBy = "Job Title";
					    var StartUpHireAscDesc = "asc";
					    var StartUpHireShowJobs = <?php the_field("show_per_page"); ?>;
					    var StartUpHireCSS = "<?php bloginfo("template_directory"); ?>/css/jobs.css";
					</script>
					<script src="https://www.startuphire.com/javascript/widgetSetup.js" type="text/javascript" language="javascript"></script>
					<div id="poweredByStartUpHire"><a href="http://www.startuphire.com/j/US/Engineering-jobs">Engineering Jobs</a> powered by StartUpHire</div>

				</div>
				

			</div>
		</div>
	</div>

</section>