<?php
/**
 * Template Name: Team Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage revel
 * @since revel 1.0
 */

?>
<section id="<?php the_field("section_id"); ?>" class="team-page">
	<div class="container-fluid">
		<div class="row parallax-wrapper">
			<?php
                $img = wp_get_attachment_image_src(get_post_thumbnail_id(), "full");
                $imgDir = esc_url( get_template_directory_uri() ) . '/images/team/';
            ?>
			<div class="parallax team-intro section-intro">

				<div class="clipper">
		        	<div class="bg-image" ></div>
		        </div>
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-8 col-xs-12 bottom-text">
							<h4><?php the_field("subtitle"); ?></h4>
							<h2 class="section-title"><?php the_title(); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $the_query = get_team(); ?>
	<?php if( $the_query->have_posts() ): ?>
		<div class="description-slider team-slider" id="team-slider">
			<a href="" class="close-slider"></a>
			<ul class="team-slider-ul ">
			<?php while( $the_query->have_posts() ): $the_query->the_post(); ?>
				<li data-title="<?php the_title(); ?>">
					<div class="container-fluid">
						<div class="row">
							<?php
								if( get_field( "big_image" ) ){
									$img = wp_get_attachment_image_src( get_field( "big_image" ), "team-big" );
								}else{
									$img = wp_get_attachment_image_src( get_post_thumbnail_id(), "team-big" );
								}
							?>
							<div class="col-lg-6 col-md-12  team-image" style="background-image: url('<?php echo $img[0]; ?>');">
							</div>
							<div class="col-lg-5 col-md-12 background">
								<h3><?php the_title(); ?></h3>
								<h5><?php _e("Background"); ?></h5>
								<?php the_content(); ?>
								<?php if( have_rows("investments") ): ?>
									<div class="investments">
										<h5><?php _e("Investments"); ?></h5>
									<?php while( have_rows("investments") ): the_row(); ?>
                                        <a target="_blank" class="investment-link" href="<?php the_sub_field("investment_link"); ?>">
										    <span><?php the_sub_field("investment"); ?></span>
                                        </a>
									<?php endwhile; ?>
									</div>
								<?php endif; ?>

							</div>
						</div>
					</div>
				</li>
			<?php endwhile; ?>
			</ul>
		</div>
	<?php endif; ?>


	<?php $the_query = get_team(); ?>
	<?php if( $the_query->have_posts() ): ?>
		<div class="container team-members">
			<div class="row">
			<?php while( $the_query->have_posts() ): $the_query->the_post(); ?>
				<div class="col-md-4 col-sm-6 team-member">
					<a href="#" class="image">
						<?php the_post_thumbnail("team-small"); ?>
					</a>
					<a href="#" class="title"><?php the_title(); ?></a>
					<?php  if( get_field("linkedin_url") ): ?>
					<a class="linkedin" href="<?php the_field("linkedin_url"); ?>" target="_blank"></a>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>


</section>