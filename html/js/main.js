(function($) {
    $.each(['show','hide'], function(i, val) {
        var _org = $.fn[val];
        $.fn[val] = function() {
            this.trigger(val);
            _org.apply(this, arguments);
        };
    });
})(jQuery);

$(document).ready(function(){
    var page1FirstTime=true;
    var prePage=1;
    $('#fullpage').fullpage({
        anchors: ['0Page', '1Page', '2Page', '3Page', '4Page', '5Page', '6Page'],
        menu: '#menu',
        //scrollOverflow: true,
            autoScrolling: false,
        fitToSection: false,

        onLeave:function(index, nextIndex, direction){
            prePage=index;
            //Hide side menu when move to home page
            if(nextIndex===1){
                $('.push').removeClass('toggled');
                $('#side-menu').removeClass('toggled');
                $('#side-menu-open-btn').hide();
            }
        },

        afterLoad: function(anchorLink, index){
            var loadedSection = $(this);

            //Show side menu when move down to page about us
            if(prePage===1 && index===2){
                //$('body').addClass('toggled');
                $('#side-menu-open-btn > a').click();
                $('#side-menu-open-btn').hide();
            }
            //Hide side menu when move down from page about us
            if((prePage===2 || prePage===1) && index!==1 && index!==2){
                $('.push').removeClass('toggled');
                $('#side-menu').removeClass('toggled');
                $('#side-menu-open-btn').show();
            }

            //Navigation on side menu
            if(index!==1){
                $('#side-menu-nav > div > ul > li').css('border-right','0px');
                $('#side-menu-nav > div > ul > li').css('padding-right','40px');
                var temp='#side-menu-nav > div > ul > li:eq('+(Number(index)-2)+')';
                $(temp).css('border-right','solid #7bbacb 4px');
                $(temp).css('padding-right','36px');

            }

            //Change image for Scrolling Btn
            if(index!==1){
                $('#scrolling-btn').attr('data-menuanchor','0Page');
                $('#scrolling-btn > a').attr('href','#0Page');
                $('#scrolling-btn > a > img').attr('src','images/scrolling_arrow_up.png');
            }else{
                $('#scrolling-btn').attr('data-menuanchor','1Page');
                $('#scrolling-btn > a').attr('href','#1Page');
                $('#scrolling-btn > a > img').attr('src','images/scrolling_arrow.png');
            }

            //Animation Number
            if(index === 2 && page1FirstTime===true){
                page1FirstTime=false;
                $('.page02_col_01 > ul > li:eq(0) > span').animateNumber({ number: 68 },2000);

                var plus_number_step = $.animateNumber.numberStepFactories.append('+');
                $('.page02_col_01 > ul > li:eq(1) > span').animateNumber(
                    {
                        number: 12,
                        numberStep: plus_number_step
                    },
                    2000
                );

                var decimal_places = 1;
                var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);
                $('.page02_col_01 > ul > li:eq(2) > span').animateNumber(
                    {
                        number: 2.6 * decimal_factor,

                        numberStep: function(now, tween) {
                            var floored_number = Math.floor(now) / decimal_factor,
                                target = $(tween.elem);

                            if (decimal_places > 0) {
                                // force decimal places even if they are 0
                                floored_number = floored_number.toFixed(decimal_places);

                                // replace '.' separator with ','
                                floored_number = floored_number.toString();
                            }

                            target.text('$' + floored_number + 'M');
                        }
                    },
                    2000
                );
            }
        },
    });

    //Home page Slide
    $('.my-slider').slick({
            dots:true,
            autoplay: true,
            autoplaySpeed: 5000,
        }
    );
    var pos='.slide-text:eq('+1+')>span';
    $(pos).fadeIn(1000);
    $('.my-slider').on('afterChange',function(event, slick, currentSlide){
        pos='.slide-text:eq('+(currentSlide+1)+')>span';
        $(pos).fadeIn(1000);
    });
    $('.my-slider').on('beforeChange',function(event, slick, currentSlide, nextSlide){
        pos=nextSlide===2?('.slide-text:eq('+1+')>span'):('.slide-text:eq('+(nextSlide+2)+')>span');
        $(pos).css('display','none');
    });

    //Create side menu
    $('.easy-sidebar-toggle').click(function(e) {
        e.preventDefault();
        $('.push').toggleClass('toggled');
        $('#side-menu').toggleClass('toggled');
        $('#side-menu-open-btn').toggle();
    });


    //Portfolio Slide
    $('.page03-row-02-slides').slick({
        adaptiveHeight: true,
        infinite: true,
    });
    //Portfolio slide binding data
    $('.portfolio-block').each(function(){
        var pos='.portfolio-slide-content:eq('+($(this).index()+1)+')>ul>li>';
        var data_ceo=$(this).children('.data-ceo').html();
        var data_location=$(this).children('.data-location').html();
        var data_website=$(this).children('.data-website').html();
        var data_summary=$(this).children('.data-summary').html();
        $(pos+'.portfolio-slide-ceo').html(data_ceo);
        $(pos+'.portfolio-slide-loc').html(data_location);
        $(pos+'.portfolio-slide-web').html(data_website);
        $('.portfolio-slide-content:eq('+($(this).index()+1)+')>p>.portfolio-slide-sum').html(data_summary);
    })
    $('.portfolio-block').click(function(e){
        e.preventDefault();
        $('.page03-row-02').show();
        $('.page03-row-02-slides').slick('slickGoTo',$(this).index());
        $('html, body').animate({
            scrollTop: $(".page03-row-02-slides").offset().top
        }, 500);
        var temp=$('.page03-row-02-slides>div>div>div').height();
        console.log(temp);
    });
    $('.portfolio-slide-close-btn > a').click(function(e){
        e.preventDefault();
        $('.page03-row-02').hide();
    });
    //Portfolio Slide - Change name on arrow
    $('.page03-row-02-slides').on('afterChange',function(event, slick, currentSlide){
        var prePath='.page03-row-02-slides > div > div > div:eq('+currentSlide+')';
        var nexPath='.page03-row-02-slides > div > div > div:eq('+(Number(currentSlide) + 2)+')';
        var preName = $(prePath).attr('name');
        var nexName = $(nexPath).attr('name');
        var left=$('.portfolio-slide-left-name > a').html(preName);
        var right=$('.portfolio-slide-right-name > a').html(nexName);
    });
    $('.portfolio-slide-left-name').click(function(e){
        e.preventDefault();
        $('.page03-row-02-slides').slick('slickPrev');
        $('html, body').animate({
            scrollTop: $(".page03-row-02-slides").offset().top
        }, 500);
    })
    $('.portfolio-slide-right-name').click(function(e){
        e.preventDefault();
        $('.page03-row-02-slides').slick('slickNext');
        $('html, body').animate({
            scrollTop: $(".page03-row-02-slides").offset().top
        }, 500);
    })

    //Team Slide
    $('.page04-row-02-slides').slick();
    //Team slide binding data
    $('.team-block').each(function(){
        var pos='.team-slide-content:eq('+($(this).index()+1)+')>';
        var data_name=$(this).children('.data-name').html();
        var data_background=$(this).children('.data-background').html();
        var data_intvestments=$(this).children('.data-investments').html().split(',');
        var investmentsList="";
        for(var i=0;i<data_intvestments.length;i++){
            investmentsList+='<li>'+data_intvestments[i]+'</li>';
        }
        $(pos+'.team-slide-nam').html(data_name);
        $(pos+'.team-slide-bac').html(data_background);
        $(pos+'.team-slide-int').html(investmentsList);
    })
    $('.team-block').click(function(e){
        e.preventDefault();
        $('.page04-row-02').show();
        $('.page04-row-02-slides').slick('slickGoTo',$(this).index());
        $('html, body').animate({
            scrollTop: $(".page04-row-02-slides").offset().top
        }, 500);
    });
    $('.team-slide-close-btn > a').click(function(e){
        e.preventDefault();
        $('.page04-row-02').hide();
    });
    //Team Slide - Change name on arrow
    $('.page04-row-02-slides').on('afterChange',function(event, slick, currentSlide){
        var prePath='.page04-row-02-slides > div > div > div:eq('+currentSlide+')';
        var nexPath='.page04-row-02-slides > div > div > div:eq('+(Number(currentSlide) + 2)+')';
        var preName = $(prePath).attr('name');
        var nexName = $(nexPath).attr('name');
        var left=$('.team-slide-left-name > a').html(preName);
        var right=$('.team-slide-right-name > a').html(nexName);
    });
    $('.team-slide-left-name').click(function(e){
        e.preventDefault();
        $('.page04-row-02-slides').slick('slickPrev');
        $('html, body').animate({
            scrollTop: $(".page04-row-02-slides").offset().top
        }, 500);
    })
    $('.team-slide-right-name').click(function(e){
        e.preventDefault();
        $('.page04-row-02-slides').slick('slickNext');
        $('html, body').animate({
            scrollTop: $(".page04-row-02-slides").offset().top
        }, 500);
    })

    //Portfolio filter
    $('.page03-row-03>div>ul>li>a').click(function(e){
        e.preventDefault();
        var filter=$(this).attr('filter');
        if(filter==='ALL'){
            $('.portfolio-block').each(function(){
                $(this).css('display','inline');
            });
        }else{
            $('.portfolio-block').each(function(){
                var category=$(this).attr('category');
                if(category.indexOf(filter)>=0){
                    $(this).css('display','inline');
                }else{
                    $(this).css('display','none');
                }
            });
        }
    });


    //Side menu visible, must be loaded at last
    $('body').css('overflow','visible');
});