<?php
/**
 * Template Name: Home Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage revel
 * @since revel 1.0
 */

get_header(); the_post(); ?>

		<?php  
			$the_query = get_slides();
			if( $the_query->have_posts() ):
		?>

		<section class="slider">
			<ul class="main-bxslider">
				<?php while( $the_query->have_posts() ): $the_query->the_post(); ?>
					<?php $img = wp_get_attachment_image_src( get_post_thumbnail_id(), "full" ); ?>
					<li style="background-image: url(<?php echo $img[0]; ?>);">
						<div class="table-element">
							<div class="cell-element">
								<?php the_content(); ?>
							</div>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		</section>
		<?php
			endif;
			wp_reset_postdata();
		?>


	<?php $the_query = get_child_pages( get_the_ID() ); 
		  $i = 0;
	?>
	<?php if( $the_query->have_posts() ): ?>
		<div class="sub-sections ">
			
		<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<?php 

				if( $i == 0 ): ?>
					
				<?php endif;
				$i++;

				$template = str_replace(".php", "", basename( get_page_template() ));
				get_template_part( $template );
			

			?>
			
		<?php endwhile; wp_reset_query(); ?>
		</div>
	<?php endif; ?>

<?php get_footer(); ?>