var $ = jQuery.noConflict();

var sliders = new Array();
var counterLounched = false;
var portfolioSlider;
var mobileWidth = 768;
var portfolioDetailSmallImageWidth = 240;
var mobileSliderSpeed = 1;
var windowWidth = $(window).width();

var Project = {

	init: function()
	{
		$('html').removeClass('no-js');
		Project.initFormFields();
		Project.initSliders();
		Project.smoothScroll( null );
		Project.backToTopButton();
		Project.calculateAboutBg();
		Project.leftMenu();
		Project.portfolio();
		Project.menuWalker();
		Project.initTweetie();
		Project.showMenu();
		Project.fixIframe();
		Project.hideCrunchBaseIcon();
        Project.teamBg();
		Project.portfolioDescription();
		Project.teamDescription();
		Project.contactDescription();
	},
	contactDescription: function() {
		var descriptionHeight = $('.contact-page .contact-intro .bottom-text').height();
		$('.contact-page .contact-intro').height(descriptionHeight + 50 > 448 ? descriptionHeight + 50 : 448);
	},
	portfolioDescription: function() {
		var descriptionHeight = $('.portfolio-page .portfolio-intro .bottom-text').height();
		$('.portfolio-page .portfolio-intro').height(descriptionHeight + 50);
	},
	teamDescription: function() {
		if(windowWidth <= mobileWidth) {
			var descriptionHeight = $('.team-page .team-intro .bottom-text').height();
			$('.team-page .team-intro .bottom-text').css('bottom', 50);
		}
	},
	hideCrunchBaseIcon: function() {
		if($('.icons-crunchbase').length > 0) {
			$('.icons-crunchbase').closest('li').css('display', windowWidth <= mobileWidth ? 'none' : 'block');
		}
	},
	doCalculations : function(){
		Project.initSections();
		Project.calculateAboutBg();
		Project.calculateBackToTopIcon();

		$('.portfolio-grid').isotope( 'reloadItems' ).isotope();

	},
	initFormFields : function()
	{
		$('input[type="text"], input[type="email"], input[type="tel"], input[type="password"], textarea').focus(function() { 
			if ($(this).val() === $(this).prop('defaultValue')) {
				$(this).val('');
			}
		});
		
		$('input[type="text"], input[type="email"], input[type="tel"], input[type="password"], textarea').blur(function() {
			if ($(this).val() === '') {
				$(this).val($(this).prop('defaultValue'));
			} 
		});
	},
	showTitle : function( index ){
		/*setTimeout(function(){
			$("section.slider .main-bxslider li").eq(index).find("h3").fadeIn(1000);
		}, 1000);*/
		$(".main-bxslider li").eq(index).find("h3").show();
	},
	initSliders : function() {

		// Home slider
		var slider = $(".main-bxslider").bxSlider({
			auto: true,
			pause: 7000,
			speed: 1500,
            useCSS: true,
            easing: 'ease-in-out',
            swipeThreshold: 100,
			onSliderLoad : function(currentIndex){
				setTimeout(function(){
					$("section.slider .main-bxslider li").eq(1).find("h3").css("opacity", 1);
				}, 1000);

			},
			onSlideAfter : function($slideElement, oldIndex, newIndex){
				$($slideElement.prevObject[oldIndex]).find("h3").css("opacity", 0);
				setTimeout(function(){
					$slideElement.find("h3").css("opacity", 1);
				}, 1000);

				//Project.showTitle(newIndex);

			}
		});

		sliders.push( slider );


		// Portfolio Slider
		var portfolioBxSlider = {
			pager: false,
			swipeThreshold: 100,
			onSliderLoad : function(currentIndex){
				Project.portfolioSliderTitles( currentIndex );
				if(windowWidth <= mobileWidth) {
					$('#portfolio-slider .bx-controls').hide();
					$('.attachment-portfolio-big').attr('width', portfolioDetailSmallImageWidth);
				} else {
					$('#portfolio-slider .bx-controls').show();
				}
			},
			onSlideAfter : function($slideElement, oldIndex, newIndex){
				Project.portfolioSliderTitles( newIndex );
			}
		};
		portfolioBxSlider.touchEnabled = windowWidth > mobileWidth;
		portfolioBxSlider.speed = windowWidth > mobileWidth ? 500 : mobileSliderSpeed;
		portfolioSlider = $(".portfolio-slider-ul:not(.prototype)").bxSlider(portfolioBxSlider);
		$(".portfolio-slider").hide();
		//$(".portfolio-slider").hide();
		sliders.push(portfolioSlider);

		// Team Slider
		var teamBxSlider = {
			pager: false,
			swipeThreshold: 100,
			onSliderLoad : function(currentIndex){
				Project.teamSliderTitles( currentIndex );
				if(windowWidth <= mobileWidth) {
					$('#team-slider .bx-controls').hide();
				} else {
					$('#team-slider .bx-controls').show();
				}
			},
			onSlideAfter : function($slideElement, oldIndex, newIndex){
				Project.teamSliderTitles( newIndex );
			}
		};
		teamBxSlider.touchEnabled = windowWidth > mobileWidth;
		teamBxSlider.speed = windowWidth > mobileWidth ? 500 : mobileSliderSpeed;
		teamSlider = $(".team-slider-ul:not(.prototype)").bxSlider(teamBxSlider);
		$(".team-slider").hide();
		sliders.push(teamSlider);

		//hide on click
		$(".portfolio-slider .close-slider").on("click", function(e){
			e.preventDefault();

			$(".portfolio-page .filters").removeClass("slider-open");
			$(".portfolio-slider").slideUp(windowWidth > mobileWidth ? 300: 0);
		});

		$(".team-slider .close-slider").on("click", function(e){
			e.preventDefault();
			$(".team-slider").slideUp(windowWidth > mobileWidth ? 300: 0);
		});

		//show on click
		$(".portfolio-element .content").click( function(e){

			$(".portfolio-page .filters").addClass("slider-open");
			var index = $(this).parent().index();
			$(".portfolio-slider").slideDown(windowWidth > mobileWidth ? 300: 0, function(){
				exeCallBack = windowWidth > mobileWidth ? portfolioSliderCallBack() : null;
			});
			exeCallBack = windowWidth <= mobileWidth ? portfolioSliderCallBack() : null;

			function portfolioSliderCallBack() {
				portfolioSlider.reloadSlider();
				portfolioSlider.goToSlide( index  );
				Project.smoothScroll( "#portfolio-slider", windowWidth <= mobileWidth );
			}
			e.preventDefault();
			
			/*$(".portfolio-slider").animate({
				"margin-top" : 0 
			}, 500, function(){
				$(".portfolio-slider").css("z-index" , 0 );
				//Project.smoothScroll("#portfolio-slider");
			});*/
		} );

		$(".team-member a:not(.linkedin)").click( function(e){

			//$(".portfolio-page .filters").addClass("slider-open");

			var index = $(this).parent().index();
			$(".team-slider").slideDown(windowWidth > mobileWidth ? 300: 0, function(){
				exeCallBack = windowWidth > mobileWidth ? teamSliderCallBack() : null;
			});
			exeCallBack = windowWidth <= mobileWidth ? teamSliderCallBack() : null;

			function teamSliderCallBack() {
				teamSlider.reloadSlider();
				teamSlider.goToSlide( index  );
				Project.smoothScroll( "#team-slider", windowWidth <= mobileWidth );
			}
			e.preventDefault();
			/*$(".team-slider").animate({
				"margin-top" : 0 
			}, 500, function(){
				//Project.smoothScroll("#portfolio-slider");
				$(".team-slider").css("z-index" , 0 );
			});*/
			
		} );


		Project.initSections();

	},

	portfolioSliderTitles : function( index ){
		var prevInd = 0;
		var nextInd = 0;
		if( $(".portfolio-slider-ul li").length > 1 ){
			if( index == 0 ){
				prevInd = 0;
				nextInd = 2;
			}else{
				prevInd = index ;
				nextInd = index + 2;
			}
		}
		$(".portfolio-page .portfolio-slider .bx-controls-direction .bx-prev").text( $(".portfolio-slider-ul li").eq( prevInd ).data("title") );
		$(".portfolio-page .portfolio-slider .bx-controls-direction .bx-next").text( $(".portfolio-slider-ul li").eq( nextInd ).data("title") );
		//$(".portfolio-slider-ul li").
	},

	teamSliderTitles : function( index ){
		var prevInd = 0;
		var nextInd = 0;
		if( $(".portfolio-slider-ul li").length > 1 ){
			if( index == 0 ){
				prevInd = 0;
				nextInd = 2;
			}else{
				prevInd = index ;
				nextInd = index + 2;
			}
		}
		$(".team-page .team-slider .bx-controls-direction .bx-prev").text( $(".team-slider-ul li").eq( prevInd ).data("title") );
		$(".team-page .team-slider .bx-controls-direction .bx-next").text( $(".team-slider-ul li").eq( nextInd ).data("title") );
		//$(".portfolio-slider-ul li").
	},

	smoothScroll : function( id, fast ){
		if( id ){
			if( id != "#" ){
				var offset = $( id ).offset().top ;
				if( offset != 0 ){
					offset = offset + 1;
				}
				if($.browser.safari) bodyelem = $("body")
				else bodyelem = $("html,body")

				bodyelem.stop(true, true).animate( {
					scrollTop : offset + "px"
				}, fast ? 1 : 1000 );
			}
		}else{
			$("a[href*=#]").click( function( e ){
				e.preventDefault();
				var id = $(this).attr("href");
				if( id != "#" ){
					Project.smoothScroll( id );
				}
			} );
		}
	},
	initSections : function(){
		var fullHeight = $(window).height();
		var he = (fullHeight * 96) / 100;
		$("section.slider").css("height", fullHeight + "px");
		$("section.slider .main-bxslider").css("height", he + "px");
		$(".full-height").css("height", fullHeight + "px");

		$(".full-height-col-1").css("height", fullHeight + "px");
		$(".full-height-col-2").css("height", (fullHeight/2) + "px");
		$(".full-height-col-3").css("height", (fullHeight/3) + "px");
		$(".full-height-col-4").css("height", (fullHeight/4) + "px");


		var h = $(".about-page .about-content").outerHeight();
		if(windowWidth > mobileWidth) {
			$(".about-page .about-bg").css("min-height", h + "px");
			$(".about-page-parallax").css("height", fullHeight + "px");
		}
		Project.reloadSliders();
		
	},
	reloadSliders : function(){
		$.each( sliders, function(){
			var activeSlide = this.getCurrentSlide();
			this.reloadSlider();
			this.goToSlide( activeSlide );
		} );
	},
	calculateAboutBg : function(){
		//var scrolledY = $(window).scrollTop() - $(".about-bg").offset().top;



		var height = $(window).height();
		var bottomScrean = height + $(window).scrollTop();
		var topBlock = $(".about-bg").offset().top;

		var val = height - ( bottomScrean - topBlock);
		//$('.about-bg').css('background-position', 'left ' + (-(val)) + 'px');
		if( $(window).width() < 992 ){
			$(".about-page .about-bg").css("min-height", "auto");
			$(".about-page .about-bg").css("height", "auto");
			$(".about-page-parallax").css("height", "100%");
		}else{
			
		}

		$(".about-page-parallax").css("top", (-(val)) + 'px');



		/*$(".parallax").each( function(){
			var topBlock = $(this).offset().top;
			var heightBlock = $(this).outerHeight();
			//var val = height - ( bottomScrean - topBlock);
			var val = heightBlock - (bottomScrean - topBlock);
			if( $(this).hasClass("portfolio-intro") ){
				console.debug( bottomScrean, topBlock );
			}

			$(this).css('background-position', 'left ' + (-(val)) + 'px');
		} );*/
		

  		
	},
	countingAnimate : function(){
		if( !counterLounched){
			var scrl = $(window).scrollTop() + ($(window).height() / 2);
			if( $(".count").first().offset().top < scrl ){
				counterLounched = true;
				$('.count').each(function () {
				    $(this).prop('Counter',0).animate({
				        Counter: $(this).data("count")
				    }, {
				        duration: 2000,
				        easing: 'swing',
				        step: function (now) {
                            var p = Math.ceil(now * 10) / 10;
				            $(this).text(p.toLocaleString());

				        }
				    });
				});
			}
		}
	},
	backToTopButton : function(){
		$(".back-to-top").unbind().click( function(e){
			e.preventDefault();
			e.stopPropagation();
			if( $(window).scrollTop() > ( $(window).height() / 2) ){
				// back to top
				Project.smoothScroll("#page");
			}else{
				// scroll to second section
				var id = $(".sub-sections section").first().attr("id");
				Project.smoothScroll( "#" + id );
			}
		} );
	},
	calculateBackToTopIcon : function(){
		if( $(window).scrollTop() > ( $(window).height() / 2) ){
			// back to top
			$(".back-to-top").addClass("active");
		}else{
			// scroll to second section
			//$("body").removeClass("pushed-to-right");
			//$(".left-menu-push").removeClass("active");
			$(".back-to-top").removeClass("active");
		}

		// calculate hamburger icon
		if( $(window).scrollTop() >= ( $(window).height()) ){
			$(".menu-hamburger").css("z-index", 999999);
			$(".left-menu-push").addClass("fixed");
			$("header.main").hide();
		}else{
			$(".menu-hamburger").css("z-index", 0);
			$(".left-menu-push").removeClass("fixed");
			$("header.main").show();
		}

	},
	leftMenu : function(){

		//init menu

		//$(".left-menu-push").addClass("active");
		//$(".sub-sections").addClass("pushed-to-right");

		$(".menu-hamburger").click( function(e){
			e.preventDefault();
			$(".sub-sections").addClass("pushed-to-right");
			$(".left-menu-push").addClass("active");
			setTimeout( function(){
				Project.reloadSliders();
			}, 400 );
			
			//Project.doCalculations();
		} );
		$(".close-menu").click( function(e){
			e.preventDefault();
			$(".sub-sections").removeClass("pushed-to-right");
			$(".left-menu-push").removeClass("active");
			setTimeout( function(){
				Project.reloadSliders();
				$('.portfolio-grid').isotope( 'reloadItems' ).isotope();
			}, 400 );
			//Project.doCalculations();
		} );

		$(".left-menu-push .menu a").click(function(){
			$(".sub-sections").removeClass("pushed-to-right");
			$(".left-menu-push").removeClass("active");
			setTimeout( function(){
				Project.reloadSliders();
				$('.portfolio-grid').isotope( 'reloadItems' ).isotope();
			}, 400 );
		});
	},
	portfolio : function(){
		// run isotope
		var grid = $('.portfolio-grid').isotope({
			itemSelector: '.portfolio-element'
		});

		//map click on filter
		$(".portfolio-page .filters a").click( function(e){
			e.preventDefault();
			var filterValue = $(this).attr('data-filter');
  			grid.isotope({ filter: filterValue });
  			
  			$(this).addClass("active");
  			$(this).parent().siblings().find("a").removeClass("active");
		} );
	},
	menuWalker : function(){
		var numer = 0;
		var i = 0;
		var scrlTop = $(window).scrollTop();
		$(".left-menu-push ul li").each( function(){
			var id = $(this).children("a").attr("href");
			if( id != "#" && $(id).length ){
				var sectionTop = $(id).offset().top;
				if( scrlTop >= (sectionTop) ){
					numer = i;
				}else{
					if( numer != 0 ){
						return;
					}
				}
			}
			i++;
		} );
		var marginTop = 24 + ( numer * 50 ); 
		$(".left-menu-push .menu-walker").css("margin-top", marginTop + "px");
	},
	initTweetie : function(){

		if( $(".twitter-container").length ){
			var count = $(".twitter-container").data("count");
			var path = $(".twitter-container").data("path");

			$('.twitter-container').twittie({
			    'counter'			: count,
			    'count'			: 99999,
			    'apiPath'		: SiteVars.ajaxUrl,
			    'dateFormat'	: "<span class=\"number\">%d</span>%b",
			    'template' 		: "<div class=\"news-item col-sm-{{col}} col-xs-12 col-md-{{col}}\"><a href=\"{{url}}\" class=\"image\" style=\"background-image: url('{{image_src}}'); height: {{image_height}}px;width: {{image_width}}px;\"><span class=\"date\">{{date}}</span></a>{{tweet}}</div>",
			    'hideReplies'	: true
			}, function() {
			    $('.twitter-container').isotope({
					itemSelector: '.news-item',
					percentPosition: true,
					masonry: {
					  columnWidth: '.col-md-3'
					}
				});
			});
		}
	},
	showMenu : function(){
		if( $(window).width() < 1200 ){
			$(".sub-sections").removeClass("pushed-to-right");
			$(".left-menu-push").removeClass("active");
			setTimeout( function(){
				Project.reloadSliders();
			}, 500 );
			
		}

		if( $(window).width() < 992 ){
			$(".full-height ").css("height", "auto");
		}else{
			Project.initSections();
		}
	},
	fixIframe : function(){
		if( $(".jobs-page .iframe-wrapper iframe").length && $(".jobs-page .iframe-wrapper iframe").attr("src") != "" ){
			var src = $(".jobs-page .iframe-wrapper iframe").attr("src");
			var src_old = src;
			var id = $(".portfolio-page").attr("id");
			src = src.split( "&" );
			var len = src.length - 1;
			src = src[len].split("=");
			var old = src[1];
			src = src[1].split("%23");
			src = src[0] + "%23" + id;
			var new_src = src_old.replace(old, src);
			$(".jobs-page .iframe-wrapper iframe").attr("src", new_src);
		}else{
			setTimeout( function(){
				Project.fixIframe();
			}, 500 );
		}
	},
    teamBg : function(){
        var w = $(window).width();
        var h = $(window).height();
        var h1 = $(".team-page").height();

		if(w <= mobileWidth) {
			$(".team-page .team-intro .clipper").css('background-attachment', 'scroll');
		} else {
			$(".team-page .team-intro .clipper").css('background-attachment', 'fixed');
		}
    }
}

jQuery(document).ready(
	function()
	{
		Project.init();
	}
);

jQuery(window).resize(
	function()
	{
		windowWidth = $(window).width();
		Project.doCalculations();
		Project.showMenu();
		Project.teamBg();
		Project.hideCrunchBaseIcon();
		Project.portfolioDescription();
		Project.teamDescription();
		Project.contactDescription();
	}
);

jQuery(window).scroll(
	function()
	{
		Project.calculateAboutBg();
		Project.calculateBackToTopIcon();
		Project.countingAnimate();
		Project.menuWalker();
	}
);



