<?php
/**
 * RVP Mobile functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 */

if ( ! function_exists( 'rvp_mobile_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since EDF 1.0
 */
function rvp_mobile_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on rvp, use a find and replace
	 * to change 'rvp' to the name of your theme in all the template files
	 */
	//load_theme_textdomain( 'rvp', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
    if ( function_exists( 'add_theme_support' ) ) {
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 150, 150, true );
    }

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'rvp' ),
		'social'  => __( 'Social Links Menu', 'rvp' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );
}
endif; // rvp_mobile_setup
add_action( 'after_setup_theme', 'rvp_mobile_setup' );


/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since EDF 1.1
 */
function rvp_mobile_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'rvp_mobile_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since EDF 1.0
 */
function rvp_mobile_scripts() {

}
add_action( 'wp_enqueue_scripts', 'rvp_mobile_scripts' );


/**
 * Register two widget areas.
 *
 */
function rvp_mobile_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Main Widget Area', 'rvp' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Appears in the footer section of the site.', 'rvp' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name' => 'Footer - CONTACT',
        'id' => 'footer-contact',
        'description' => 'Appears in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => 'Footer Copyright',
        'id' => 'footer-copyright',
        'description' => 'Appears in the footer copyright area',
        'before_widget' => '<h6>',
        'after_widget' => '</h6>',
        'before_title' => '<h3 class="hidden">',
        'after_title' => '</h3>',
    ) );
    register_sidebar( array(
        'name' => 'Footer Menu',
        'id' => 'footer-menu',
        'description' => 'Appears in the footer menu area',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="hidden">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => 'Widgets',
        'id' => 'widgets',
        'description' => 'Appears in the widgets menu area',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="hidden">',
        'after_title' => '</h3>',
    ) );
}
add_action( 'widgets_init', 'rvp_mobile_widgets_init' );


function ms_image_editor_default_to_gd( $editors ) {
    $gd_editor = 'WP_Image_Editor_GD';

    $editors = array_diff( $editors, array( $gd_editor ) );
    array_unshift( $editors, $gd_editor );

    return $editors;
}
add_filter( 'wp_image_editors', 'ms_image_editor_default_to_gd' );

function rvp_mobile_post_nav_background() {
    if ( ! is_single() ) {
        return;
    }

    $previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
    $next     = get_adjacent_post( false, '', false );
    $css      = '';

    if ( is_attachment() && 'attachment' == $previous->post_type ) {
        return;
    }

    if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
        $prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
        $css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
    }

    if ( $next && has_post_thumbnail( $next->ID ) ) {
        $nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
        $css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
    }

    wp_add_inline_style( 'rvp-mobile-style', $css );
}
add_action( 'wp_enqueue_scripts', 'rvp_mobile_post_nav_background' );

add_filter( 'wp_image_editors', 'change_graphic_lib' );
function change_graphic_lib($array) {
    return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}
