<?php
/**
 * The footer for theme.
 *
 *
 * @package WordPress
 * @subpackage revel
 * @since revel 1.0
 */
?>
	</section> <!-- /#page --> 
<?php wp_footer(); ?>
</body>
</html>