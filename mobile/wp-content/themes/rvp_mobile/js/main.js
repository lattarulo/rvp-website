$(document).ready(function () {
    $("li.m_PageScroll2id>a,a[rel='m_PageScroll2id']").mPageScroll2id();


    $("a[rel='m_PageScroll2id']").mPageScroll2id({
    });

//    var page1FirstTime = true;
    var prePage;
    $('#fullpage').fullpage({
//              sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE', 'whitesmoke', '#ccddff'],
        anchors: ['0Page', '1Page', '2Page', '3Page', 'lastPage'],
        menu: '#menu',
        slidesNavigation: true,
        slidesNavPosition: 'bottom',
//              scrollingSpeed: 1000,

        afterLoad: function (anchorLink, index) {
            var loadedSection = $(this);

            //demo: alert("Section "+index+" ended loading ---- "+anchorLink); -----> Section 1 ended loading ---- 0Page

            //Show side menu when scroll down to page 1
            if (prePage === 1 && index === 2) {
                $('body').addClass('toggled');
                $('#side-menu-open-btn').hide();
            } else if (index === 1) {
                $('body').removeClass('toggled');
                $('#side-menu-open-btn').hide();
            }

            //Navigation on side menu
            if (index !== 1) {
                $('#side-menu-nav > div > ul > li').css('border-right', '0px');
                $('#side-menu-nav > div > ul > li').css('padding-right', '40px');
                var temp = '#side-menu-nav > div > ul > li:eq(' + (Number(index) - 2) + ')';
                $(temp).css('border-right', 'solid #7bbacb 4px');
                $(temp).css('padding-right', '36px');

            }

            //Change image for Scrolling Btn
            if (index !== 1) {
                $('#scrolling-btn').attr('data-menuanchor', (Number(index) - 2) + 'Page');
                $('#scrolling-btn > a').attr('href', '#' + (Number(index) - 2) + 'Page');
                $('#scrolling-btn > a > img').attr('src', 'images/scrolling_arrow_up.png');
            } else {
                $('#scrolling-btn').attr('data-menuanchor', '1Page');
                $('#scrolling-btn > a').attr('href', '#1Page');
                $('#scrolling-btn > a > img').attr('src', 'images/scrolling_arrow.png');
            }

            //Animation Number
//            if (index === 2 && page1FirstTime === true) {
//                page1FirstTime = false;
//                $('.page02_col_01 > ul > li:eq(0) > span').animateNumber({ number: 68 }, 2000);
//
//                var plus_number_step = $.animateNumber.numberStepFactories.append('+');
//                $('.page02_col_01 > ul > li:eq(1) > span').animateNumber(
//                    {
//                        number: 12,
//                        numberStep: plus_number_step
//                    },
//                    2000
//                );
//
//                var decimal_places = 1;
//                var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);
//                $('.page02_col_01 > ul > li:eq(2) > span').animateNumber(
//                    {
//                        number: 2.6 * decimal_factor,
//
//                        numberStep: function (now, tween) {
//                            var floored_number = Math.floor(now) / decimal_factor,
//                                target = $(tween.elem);
//
//                            if (decimal_places > 0) {
//                                // force decimal places even if they are 0
//                                floored_number = floored_number.toFixed(decimal_places);
//
//                                // replace '.' separator with ','
//                                floored_number = floored_number.toString();
//                            }
//
//                            target.text('$' + floored_number + 'M');
//                        }
//                    },
//                    2000
//                );
//            }
            prePage = index;
        },
    });
//    $('.my-slider').slick({
//            dots:true,
//            autoplay: true,
//            autoplaySpeed: 3000,
//        }
//    );
    $('.easy-sidebar-toggle').click(function (e) {
        e.preventDefault();
        $('body').toggleClass('toggled');
        $('#side-menu').removeClass('toggled');
        $('#side-menu-open-btn').toggle();
    });
    $('body').css('overflow', 'visible');
//          $('.menu-link').bigSlide();
//          $('.menu-link-close').click(function(){
//              $('.menu-link').bigSlide().bigSlideAPI.view.toggleClose();
//          });
//          $('#menu-btn').hide(0);
//          $('#sidebar-menu').children();

//    console.log($(".container").html());


    function updateNavbar(){
        if (jQuery(".navbar").offset().top < 100) {
            jQuery(".navbar-fixed-top").addClass("top-nav-collapse");
            jQuery(".navbar-fixed-top .navbar-header .navbar-brand").removeClass("hidden");
            jQuery(".navbar-default .navbar-header .navbar-toggle").addClass("header-btn-menu");
            jQuery(".navbar-default .navbar-header .navbar-toggle").removeClass("btn-menu");

        } else {
            jQuery(".navbar-fixed-top .navbar-header .navbar-brand").addClass("hidden");
            jQuery(".navbar-default .navbar-header .navbar-toggle").removeClass("header-btn-menu");
            jQuery(".navbar-default .navbar-header .navbar-toggle").addClass("btn-menu");
            jQuery(".navbar-fixed-top").removeClass("top-nav-collapse");
        }

        if( jQuery(".navbar").offset().top > sliderHeight){
            $('.navbar-default').removeClass('light-bg');
            $('img#logo-header').attr('src', templateUrl + '/images/rvp-logo.png');
        }else{
            if(currentLayerIndex){
                $('.navbar-default').addClass('light-bg');
                $('img#logo-header').attr('src', templateUrl + '/images/rvp-logo.png');
            }
        }
    }
    //Animation Number
    var page1FirstTime=true;
    function animationNumber() {
        var num_investments = $('.time-investments > ul > li:eq(0) > span').attr('data-num');
        var num_investments_year = $('.time-investments > ul > li:eq(1) > span').attr('data-num');

        if(page1FirstTime===true){
            page1FirstTime=false;
            $('.time-investments > ul > li:eq(0) > span').animateNumber({ number: num_investments },5000);

            var plus_number_step = $.animateNumber.numberStepFactories.append('+');
            $('.time-investments > ul > li:eq(1) > span').animateNumber(
                {
                    number: num_investments_year,
                    numberStep: plus_number_step
                },
                5000
            );

            var decimal_places = 1;
            var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);
            var num_time_exists = $('.time-exists > ul > li:eq(0) > span').attr('data-num');
            $('.time-exists > ul > li:eq(0) > span').animateNumber(
                {
                    number: num_time_exists * decimal_factor,

                    numberStep: function(now, tween) {
                        var floored_number = Math.floor(now) / decimal_factor,
                            target = $(tween.elem);

                        if (decimal_places > 0) {
                            // force decimal places even if they are 0
                            floored_number = floored_number.toFixed(decimal_places);

                            // replace '.' separator with ','
                            floored_number = floored_number.toString();
                        }

                        target.text('$' + floored_number + 'M');
                    }
                },
                5000
            );
        }
    }


//    jQuery(".navbar-default .navbar-header .navbar-toggle").removeClass("btn-menu");
    $(window).scroll(function (event) {
        event.preventDefault();
        updateNavbar();
        animationNumber();
//        console.log($(window).scrollTop() / $(window).height());
        // Do something
    });

    $('#menu-header').click(function(){
        var btnMenu = $('#menu-header').attr("aria-expanded");
//        console.log(btnMenu);
        if ( btnMenu === "true" || btnMenu === "undefined" ) {
            $('nav.navbar-default').removeClass("nav-background");
            jQuery(".navbar-default .navbar-header .navbar-toggle").addClass("btn-menu");
            jQuery(".navbar-default .navbar-header .navbar-toggle").removeClass("header-btn-menu");
//            $('.navbar-default').css("background", "transparent");
        } else {
            $('nav.navbar-default').addClass("nav-background");

//            $('.navbar-default').css("background", "rgba(14, 34, 54, 0.85)");
        }

    });

    function calculatePopupPosition() {
        var timeOutToSetPosition = 200;
        var viewportHeight = $(window).height();
        var teamMemberHeight = $('.team-image').height();

        var popupTop = (teamMemberHeight - viewportHeight);

//        if(popupTop < 20) popupTop = 20;
        $('.page04-row-02 .popup-team').animate({
            top: popupTop + 'px'
        }, timeOutToSetPosition );
    }


    function openPopup(element) {
        $(element).modal('show');

        setTimeout(function() {

        }, 150);
    }

    function closePopup(element) {
        $(element).modal('hide');
    }

    //Portfolio Slide
    $('.page03-row-02-slides').slick();
    $('.portfolio-block').click(function(e){

        openPopup('.portfolio-popup');
//        $('.page03-row-02').show();
        $('.page03-row-02-slides').slick('slickGoTo',$(this).index());
    });
    $('.portfolio-slide-close-btn > a').click(function(){
        $('.page03-row-02').hide();
    });

    //Close Popup Portfolio
    $('.closePopup').click(function(e){
        closePopup('.portfolio-popup');
        $('body').css('overflow', 'visible');
    });

    //Team Slide
    $('.page04-row-02').hide();
    $('.page04-row-02-slides').slick();
    $('.team-block').click(function(e){
//        $('.page04-row-02').show();
        openPopup('.team-popup');
        $('.page04-row-02-slides').slick('slickGoTo',$(this).index());
    });
    $('.portfolio-slide-close-btn > a').click(function(){
        $('.page04-row-02').hide();
    });

    //Close Popup Team
    $('.btn-close-team-popup').click(function(e){
        closePopup('.team-popup');
        $('body').css('overflow', 'visible');
    });


    //Portfolio filter
    $('.page03-row-03>div>ul>li>a').click(function(){
        var filter=$(this).attr('filter');

        if(filter==='ALL'){
            $('.portfolio-filter').each(function(){
                $(this).css('display','inline');
            });
        } else {
            $('.portfolio-filter').each(function() {
                var blockElement = $(this).find('.portfolio-block');
                var category = $(blockElement).attr('category');
                var filterElement;
                if(category.indexOf(filter)>=0){
                    filterElement = $(blockElement).closest('.portfolio-filter');
                    $(filterElement).css('display','inline');
                } else {
                    filterElement = $(blockElement).closest('.portfolio-filter');
                    $(filterElement).css('display','none');
                }
//                console.log(filterElement);
//                if(category.indexOf(filter)>=0){
//                    filterElement.css('display','inline');
//                } else {
//                    filterElement.css('display','none');
//                }
            });
        }
    });

    $('.navbar-default > ul >li').click(function(){

    });


//    $(window).scroll(function () {
//        console.log('about')
//        updateNavbar();
//    });
    var x = 0;
//    $('body').on({
//        'mousewheel': function (e) {
//
//            if (e.target.id == 'el') return;
////            console.log(e.originalEvent.wheelDelta);
//            e.preventDefault();
////            e.stopPropagation();
//            if (e.originalEvent.wheelDelta > 0) {
//                x -= $(window).height();
//            } else {
//                if (x < 0)x = 0;
//                if (x / $(window).height() == 1) {
//                    console.log('aa')
//                    x += $(window).height() / 2;
//                } else {
//
//                    x += $(window).height() / 2;
//                }
//            }
//            console.log(x);
//            $('body').scrollTop(x);
//        }
//    })
    var heightScreen;
//    jQuery( ".container" ).on( "scrollstart", function( event ) {
//        heightScreen = $(window).height();
//        console.log("Started scrolling!");
//        console.log(event);
//    });
//    $('.row').draggable();
});