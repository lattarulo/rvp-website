<?php
/**
 * Template Name: Contact Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage revel
 * @since revel 1.0
 */

?>
<section id="<?php the_field("section_id"); ?>" class="contact-page">
	<div class="container-fluid">
		<div class="row parallax-wrapper">
			<?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(), "full"); ?>
			<div class="col-md-12 parallax contact-intro section-intro">

				<div class="clipper" style="background-image: url('<?php echo $img[0]; ?>');">
		        	<div class="bg-image" ></div>
		        </div>

				<div class="container">
					<div class="row">
						<div class="col-lg-10 col-md-10 bottom-text">
							<h4><?php the_field("subtitle"); ?></h4>
							<h2 class="section-title"><?php the_title(); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<?php while( have_rows("location") ): the_row(); ?>
					<div class="location">
						<h3><?php the_sub_field("title"); ?></h3>
						<?php the_sub_field("adress"); ?>
					</div>
				<?php endwhile; ?>
			</div>

			<div class="col-lg-6 col-md-9 col-xs-12 contact-form">
				<h3><?php the_field("contact_form_title"); ?></h3>
				<?php the_field("contact_form"); ?>
			</div>
		</div>
	</div>

</section>