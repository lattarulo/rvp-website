<?php
/**
* The Header for theme.
*
* Displays all of the <head> section and page header
*
* @package WordPress
* @subpackage revel
* @since revel 1.0
*/
?>
<!DOCTYPE html>
<!--[if IE 8]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lt9"><![endif]-->
<!--[if IE 9]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9 "><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
    <!--[if lte IE 9]>
    <link href='<?php echo esc_url( get_template_directory_uri() ); ?>/css/vendor/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->
</head>
<body class="<?php echo custom_body_class(); ?>" >
	<section id="page">
		<header class="main">
			<div class="container-fluid">
				<a href="#page" class="logo">
					<img src="<?php bloginfo("template_directory"); ?>/images/logo.png" alt="<?php bloginfo("title"); ?>" />
				</a>

				<?php if( have_rows("social_icons", "option") ): ?>
					<ul class="social">
					<?php while( have_rows("social_icons", "option") ): the_row(); ?>
						<li>
							<a href="<?php the_sub_field("link"); ?>" class="icons-<?php the_sub_field("icon"); ?>" target="_blank" ><?php the_sub_field("icon"); ?></a>
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>


				<nav>
					<?php wp_nav_menu(array("theme_location" => 'primary', 'container' => false, 'items_wrap' => '<ul class="%2$s">%3$s</ul>')); ?>
				</nav>

			</div>
		</header>

		<a href="#" class="menu-hamburger"></a>

		<nav class="left-menu-push ">
			<a href="#" class="close-menu"></a>
			<a href="#page" class="logo">
				<img src="<?php bloginfo("template_directory"); ?>/images/logo.png" alt="<?php bloginfo("title"); ?>" />
			</a>
			<div style="display:table;width:100%;height:100%;">
				<div style="display:table-cell;vertical-align:middle;">
					<span class="menu-walker"></span>
					<?php wp_nav_menu(array("theme_location" => 'primary', 'container' => false, 'items_wrap' => '<ul class="%2$s">%3$s</ul>')); ?>
				</div>
			</div>
			
		</nav>

		<a href="#" class="back-to-top"></a>