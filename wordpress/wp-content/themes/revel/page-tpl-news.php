<?php
/**
 * Template Name: News Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage revel
 * @since revel 1.0
 */

?>
<section id="<?php the_field("section_id"); ?>" class="news-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if( get_field("subtitle") ): ?>
					<h4><?php the_field("subtitle"); ?></h4>
				<?php endif; ?>
				<h2 class="section-title"><?php the_title(); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 twitter-container" data-count="<?php the_field("how_many_posts"); ?>">


			</div>
		</div>
	</div>

</section>