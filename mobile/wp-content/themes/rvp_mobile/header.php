<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <!--<link rel="stylesheet" href="<?php /*echo esc_url(get_template_directory_uri()); */?>/css/bootstrap.min.css">-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico:400' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/font-awesome.css">

<!--    <link href='--><?php //echo esc_url(get_template_directory_uri()); ?><!--/bower_components/bootstrap/dist/css/bootstrap-theme.css' rel='stylesheet' type='text/css'>-->
    <link href='<?php echo esc_url(get_template_directory_uri()); ?>/bower_components/slick.js/slick/slick.css' rel='stylesheet' type='text/css'>
    <link href='<?php echo esc_url(get_template_directory_uri()); ?>/bower_components/slick.js/slick/slick-theme.css' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='<?php echo esc_url(get_template_directory_uri()); ?>/css/header.css'>
    <link href='<?php echo esc_url(get_template_directory_uri()); ?>/css/about-us.css' rel='stylesheet' type='text/css'>
    <link href='<?php echo esc_url(get_template_directory_uri()); ?>/css/portfolio.css' rel='stylesheet' type='text/css'>
    <link href='<?php echo esc_url(get_template_directory_uri()); ?>/css/page_03.css' rel='stylesheet' type='text/css'>
    <link href='<?php echo esc_url(get_template_directory_uri()); ?>/css/team.css' rel='stylesheet' type='text/css'>
    <link href='<?php echo esc_url(get_template_directory_uri()); ?>/css/contact.css' rel='stylesheet' type='text/css'>
    <script>
        var templateUrl = '<?php echo get_template_directory_uri() ?>';
    </script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button id="menu-header" type="button" class="navbar-toggle collapsed btn-menu" data-toggle="collapse" data-target="#ev-navbar-top">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand m_PageScroll2id" href="#header-slides" rel="m_PageScroll2id"><img id="logo-header" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/rvp-logo.png"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="ev-navbar-top">
            <form id="search-form-full" class="navbar-form navbar-right search-form" role="search" style="display: none; margin-right: -200px">
                <div class="form-group">
                    <input type="text" class="search-input" placeholder="Search...">
                </div>
            </form>
            <?php wp_nav_menu( array( 'theme_location' => 'primary',
                'container' => false,
                'menu_class' => 'nav navbar-nav navbar-right',
                'menu_id' => 'primary-menu'
            ) ); ?>
            <form id="search-form-collapse" class="navbar-form navbar-right search-form" role="search" style="display: none; margin-right: -200px">
                <div class="form-group">
                    <input type="text" class="search-input" placeholder="Search...">
                </div>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

