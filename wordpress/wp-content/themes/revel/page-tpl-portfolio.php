<?php
/**
 * Template Name: Portfolio Page
 * The statict page template.
 *
 *
 * @package WordPress
 * @subpackage revel
 * @since revel 1.0
 */

?>
<section id="<?php the_field("section_id"); ?>" class="portfolio-page">
	<div class="container-fluid">
		<div class="row parallax-wrapper">
			<?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(), "full"); ?>
			<div class=" parallax portfolio-intro section-intro">

				<div class="clipper" style="background-image: url('<?php echo $img[0]; ?>');">
		        	<!-- <div class="bg-image" ></div> -->
		        </div>
				<div class="container">
					<div class="row">
						<div class="col-lg-10 col-md-10 col-xs-12 bottom-text">
							<h4><?php the_field("subtitle"); ?></h4>
							<h2 class="section-title"><?php the_title(); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $the_query = get_portfolio(); ?>
		<?php if( $the_query->have_posts() ): ?>
		<div class="description-slider portfolio-slider" id="portfolio-slider">
			<a href="" class="close-slider"></a>
			<ul class="portfolio-slider-ul ">

			<?php while( $the_query->have_posts() ): $the_query->the_post(); ?>

				<?php 
					$post_terms = wp_get_post_terms( get_the_ID(), "portfolio-category" ); 
					$slugs = array();
					foreach( $post_terms as $post_term ):
						$slugs[] = $post_term->slug;
					endforeach;
				?>
				

				<li data-title="<?php the_title(); ?>">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-3 col-md-offset-2 image">
								<div style="display:table;width:100%;height:100%;">
									<div style="display:table-cell;vertical-align:middle;" class="image-content">
										<?php if( get_field("big_image") ): ?>
											<?php echo wp_get_attachment_image( get_field("big_image"), "portfolio-big"); ?>
										<?php else: ?>
											<?php the_post_thumbnail("big_image"); ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<div class="col-md-5 descritpion">
								<?php if( get_field("ceo") ): ?>
								<div class="item ceo">
									<strong><?php _e("CEO", "revel"); ?>: </strong><?php the_field("ceo"); ?>
								</div>
								<?php endif; ?>
								<?php if( get_field("location") ): ?>
								<div class="item location">
									<strong><?php _e("Location", "revel"); ?>: </strong><?php the_field("location"); ?>
								</div>
								<?php endif; ?>
								<?php if( get_field("website_title") ): ?>
								<div class="item website">
									<?php if( get_field("website_url") ): ?>
										<strong><?php _e("Website", "revel"); ?>: </strong><a href="<?php the_field("website_url"); ?>" target="_blank"><?php the_field("website_title"); ?></a>
									<?php else: ?>
										<?php the_field("website_title"); ?>
									<?php endif; ?>
								</div>
								<?php endif; ?>
								<div class="content">
									<?php the_content(); ?>
								</div>
							</div>
						</div>
					</div>
				</li>




			<?php  endwhile;?>
			</ul>
		</div>
	<?php endif;
	wp_reset_postdata();
	?>


	<div class="container">
		<div class="row">
			<div class="col-md-12 filters">
				<h5><?php _e("Filter", "revel"); ?>: </h5>
				
				<?php 
					$terms = get_terms("portfolio-category", array('orderby'=> 'description'));
					if( count( $terms ) ):	
				?>
					<ul>
						<li>
							<a href="#" data-filter="*" class="active" ><?php _e("All", "revel"); ?></a>
						</li>
						<?php foreach( $terms as $term ): ?>
						<li>
							<a href="#" data-filter=".<?php echo $term->slug; ?>" ><?php echo $term->name; ?></a>
						</li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
		<?php $the_query = get_portfolio(); ?>
		<?php if( $the_query->have_posts() ): ?>
		<div class="row portfolio-grid">

			<?php while( $the_query->have_posts() ): $the_query->the_post(); ?>

				<?php 
					$post_terms = wp_get_post_terms( get_the_ID(), "portfolio-category" ); 
					$slugs = array();
					foreach( $post_terms as $post_term ):
						$slugs[] = $post_term->slug;
					endforeach;
				?>
				<div class="col-md-4 col-sm-6 col-xs-12 portfolio-element <?php echo implode(" ", $slugs); ?>">
					
					<a class="content" href="#">
						<?php if( get_field("add_badge") ): ?>
							<div class="portfolio-badge">
								<?php the_field("badge_text"); ?>
							</div>
						<?php endif; ?>
						<div style="display:table;width:100%;height:100%;">
							<div style="display:table-cell;vertical-align:middle;">
								<?php the_post_thumbnail("portfolio-small"); ?>
							</div>
						</div>
					</a>
				</div>
			<?php  endwhile;?>
		</div>
	<?php endif;
	wp_reset_postdata();
	?>
	</div>
</section>